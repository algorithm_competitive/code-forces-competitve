# https://codeforces.com/contest/1397/problem/0
def count_letter_in_string(list_str):
    result = {}
    for s in list_str:
        for i in s:
            if i in result.keys():
                result[i] += 1
            else:
                result[i] = 1

    return result


def solve_problem(num_strs, test_strs):
    len_all_str = 0
    for i in test_strs:
        len_all_str += len(i)
    if 1 <= len_all_str <= 1000:
        for key, value in count_letter_in_string(test_strs).items():
            if value % num_strs != 0:
                return "NO"
        return "YES"
    else:
        return "NO"


if __name__ == '__main__':
    n = int(input().strip())
    for i in range(n):
        num_strs = int(input().strip())
        test_strs = []
        for i in range(num_strs):
            s = input().strip()
            test_strs.append(s)
        result = solve_problem(num_strs, test_strs)
        print(result)
