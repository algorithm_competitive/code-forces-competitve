# https://codeforces.com/contest/1397/problem/B
import math


def find_c(len_list, max_num):
    tmp = max_num ** (1 / (len_list - 1))
    floor_tmp = math.floor(tmp)
    ceil_tmp = math.ceil(tmp)
    return [i for i in range(floor_tmp, ceil_tmp+1, 1)]


def solve_problem(power_sequence):
    steps = []
    if len(power_sequence) == 1:
        return power_sequence[0] - 1
    power_sequence = sorted(power_sequence)
    cs = find_c(len(power_sequence), power_sequence[-1])
    for c in cs:
        step = 0
        for i, num in enumerate(power_sequence):
            if num - c ** i > 0:
                step = step + (num - c ** i)
            else:
                step = step + (c ** i - num)
        steps.append(step)
    return min(steps)


if __name__ == '__main__':
    n = int(input().strip())
    if 3 <= n <= 10 ** 5:
        check_conditions = True
        power_sequence_str = input().strip().split(" ")
        power_sequence = [int(i) for i in power_sequence_str]
        for i in power_sequence:
            if i < 1 or i > 10 ** 9:
                check_conditions = False
        if check_conditions:
            result = solve_problem(power_sequence)
        else:
            result = 0
    else:
        result = 0
    print(result)
