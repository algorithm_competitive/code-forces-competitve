# https://codeforces.com/contest/1397/problem/D

def find_n(num_stones):
    min_stone = min(num_stones)
    num_stones = list(filter(lambda a: a != min_stone, num_stones))
    return num_stones


def solve_piles_of_stones(num_piles, num_stones):
    if num_piles == 1:
        return "T"
    if sum(num_stones) % 2 == 1:
        return "T"
    else:
        return "HL"
    # if num_piles == 2:
    #     return "HL"
    # else:
    #     n = len(num_stones)
    #     while n > 2:
    #         num_stones = find_n(num_stones)
    #         n = len(num_stones)
    #     if n == 1:
    #         return "T"
    #     if  n == 2:
    #         return "HL"


if __name__ == '__main__':
    try_time = int(input().strip())
    for i in range(try_time):
        n = int(input().strip())
        num_stones = input().strip().split(" ")
        num_stones = [int(i) for i in num_stones]
        result = solve_piles_of_stones(n, num_stones)
        print(result)
