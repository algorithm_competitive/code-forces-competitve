




if __name__ == '__main__':
    n = int(input().strip())
    for i in range(n):
        try_time = input().strip().split(" ")
        try_time = [int(i) for i in try_time]
        result = solve_palindrome_problem(try_time)
        print(result)