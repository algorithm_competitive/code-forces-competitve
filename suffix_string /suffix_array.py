# Limited memory

from operator import itemgetter


def solve_problem(try_value):
    suffix_str = []
    count = 0
    for i in range(len(try_value)):
        suffix_str.append([count, try_value[i:]])
        count += 1
    suffix_str.append([count, ""])
    suffix_str = sorted(suffix_str, key=itemgetter(1))
    return [i[0] for i in suffix_str]


if __name__ == '__main__':
    try_value = input().strip()
    result = solve_problem(try_value)
    for i, r in enumerate(result):
        if i != len(result) - 1:
            print(r, end=" ")
        else:
            print(r)
