# https://codeforces.com/contest/231/problem/A
# brute force, greedy

def get_number_of_friend_sure_answer(item):
    count = 0
    for i in item:
        if i == 1:
            count += 1
    return count


def solve_problem_team(questions):
    count_question = 0
    for item in questions:
        num_friend = get_number_of_friend_sure_answer(item)
        if num_friend > 1:
            count_question += 1

    return count_question


if __name__ == '__main__':
    n = int(input().strip())
    questions = []
    for i in range(n):
        temp = input().strip().split(" ")
        questions.append([int(j) for j in temp])

    result = solve_problem_team(questions)
    print(result)
