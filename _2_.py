from math import sqrt


def solve_2_problem(list_num):
    max_num = max(list_num)
    new_list = []
    for i, num in enumerate(list_num):
        new_list.append(max_num - num)
    return new_list


if __name__ == '__main__':
    n = int(input().strip())
    for i in range(n):
        range_number = input().strip().split(" ")
        range_nums = [int(i) for i in range_number]
        try_time = input().strip().split(" ")
        try_time = [int(i) for i in try_time]
        # result = solve_2_problem(try_time)
        for i in range(range_nums[1]):
            try_time = solve_2_problem(try_time)
        for i in try_time:
            print(i, end=" ")
        print()
