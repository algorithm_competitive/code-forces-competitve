# https://codeforces.com/contest/1392/problem/A
# greedy, math

def solve_password_problem(list_num):
    i = list_num[0]
    for k in list_num:
        if k != i:
            return 1
    return len(list_num)


if __name__ == '__main__':
    n = int(input().strip())
    for i in range(n):
        range_number = input()
        try_time = input().strip().split(" ")
        try_time = [int(i) for i in try_time]
        result = solve_password_problem(try_time)
        print(result)
