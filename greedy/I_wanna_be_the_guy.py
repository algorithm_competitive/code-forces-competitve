# https://codeforces.com/problemset/problem/469/A
"""
3
1 2
2 2 3
-> Expected: Oh, my keyboard!
-> found: I become the guy.
:|
"""



def solve_game(X, n):
    X = sorted(X, reverse=False)
    if X[0] == 0:
        del X[0]

    temp = [i for i in range(1, n + 1)]
    if X == temp:
        return "I become the guy."
    else:
        return "Oh, my keyboard!"


if __name__ == '__main__':
    n = int(input().strip())
    try_time = input().strip().split(" ")
    X = [int(i) for i in try_time]
    try_time = input().strip().split(" ")
    Y = [int(i) for i in try_time]
    result = solve_game(list(set(X + Y)), n)
    print(result)
