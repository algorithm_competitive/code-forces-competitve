# https://codeforces.com/problemset/problem/50/A

def count_rectagular_square(m, n):
    return m * n


def count_maximum_dominoes(m, n):
    rec_square = count_rectagular_square(m, n)
    num = rec_square / 2
    return int(num)


if __name__ == '__main__':
    try_time = input().strip().split(" ")
    try_time = [int(i) for i in try_time]
    result = count_maximum_dominoes(try_time[0], try_time[1])
    print(result)
