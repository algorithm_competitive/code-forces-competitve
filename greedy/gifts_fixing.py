# https://codeforces.com/problemset/problem/1399/B
# Equal between every one
# eat exactly one candy from this gift (decrease ai by one);
# eat exactly one orange from this gift (decrease bi by one);
# eat exactly one candy and exactly one orange from this gift (decrease both ai and bi by one).


def count_step_candy(ai, bi, min_a, min_b):
    count = 0
    temp_a = ai - min_a
    temp_b = bi - min_b
    if temp_a < temp_b:
        count += temp_b
    else:
        count += temp_a
    return count


def solve_problem(a, b):
    min_a = min(a)
    min_b = min(b)
    count_steps = 0
    for i in range(len(a)):
        count_steps += count_step_candy(a[i], b[i], min_a, min_b)
    return count_steps


if __name__ == '__main__':
    n = int(input().strip())
    for i in range(n):
        x = input()
        try_time = input().strip().split(" ")
        a = [int(i) for i in try_time]
        try_time = input().strip().split(" ")
        b = [int(i) for i in try_time]
        result = solve_problem(a, b)
        print(result)
