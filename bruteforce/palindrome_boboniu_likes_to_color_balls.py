# https://codeforces.com/problemset/problem/1395/A
# brute force, math

def find_number_even_element(list_num):
    num_even_element = 0
    num_odd_element = 0
    for i in list_num:
        if i % 2 == 0:
            num_even_element += 1
        else:
            num_odd_element += 1
    return num_even_element, num_odd_element


def solve_palindrome_problem(list_num):
    if len(list_num) == 4:
        min_num = min(list_num[:3])
        for i in range(min_num + 1):
            num_even, num_odd = find_number_even_element(
                [list_num[0] - i, list_num[1] - i, list_num[2] - i, list_num[3] + i * 3])
            if num_odd <= 1:
                return "YES"
            if i == 2:
                break
    return "NO"

if __name__ == '__main__':
    n = int(input().strip())
    for i in range(n):
        try_time = input().strip().split(" ")
        try_time = [int(i) for i in try_time]
        result = solve_palindrome_problem(try_time)
        print(result)
