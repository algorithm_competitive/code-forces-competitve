# https://codeforces.com/problemset/problem/4/A
# brute force

def solve_problem_watermelon(n):
    if n % 2 == 0 and n > 2:
        return "YES"
    else:
        return "NO"


if __name__ == '__main__':
    n = int(input().strip())
    result = solve_problem_watermelon(n)
    print(result)
