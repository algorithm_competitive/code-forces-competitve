# https://codeforces.com/problemset/problem/236/A
# brute force, implementation, strings

def solve_boy_or_girl_problem(text):
    text = set(text)
    if len(text) == 0:
        return None
    if len(text) % 2 == 0:
        return "CHAT WITH HER!"
    else:
        return "IGNORE HIM!"



if __name__ == '__main__':
    text = input().strip()
    result = solve_boy_or_girl_problem(text)
    print(result)
