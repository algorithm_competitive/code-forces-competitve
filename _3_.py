def solve_waterslide_problem(list_num):
    count = 0
    max_num = max(list_num)
    min_num = min(list_num)
    index_max = list_num.index(max_num)
    index_min = list_num.index(min_num)
    if index_max <= index_min:
        return max_num - min_num
    elif index_max < len(list_num) - 1:
        min_local = min(list_num[index_max:])
        return max_num - min_local
    else:
        return 0


if __name__ == '__main__':
    n = int(input().strip())
    for i in range(n):
        range_number = input()
        try_time = input().strip().split(" ")
        try_time = [int(i) for i in try_time]
        result = solve_waterslide_problem(try_time)
        print(result)
